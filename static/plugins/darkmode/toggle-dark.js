var toggle = document.getElementById("dark-mode-toggle");
var darkTheme = document.getElementById("dark-mode-theme");

toggle.addEventListener("click", () => {
    print("gotcha");
    if (toggle.className === "ti ti-moon-stars") {
        setTheme("dark");
    } else if (toggle.className === "ti ti-sun") {
        setTheme("light");
    }
});

function setTheme(mode) {
    if (mode === "dark") {
        darkTheme.disabled = false;
        toggle.className = "ti ti-moon-stars";
    } else if (mode === "light") {
        darkTheme.disabled = true;
        toggle.className = "ti ti-sun";
    }
}

// the default theme is light
var savedTheme = localStorage.getItem("dark-mode-storage") || "light";
setTheme(savedTheme);

function setTheme(mode) {
    localStorage.setItem("dark-mode-storage", mode);

    // same as above
}